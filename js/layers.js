/*
 * retreives layer data from wms servers
 * creates layer objects, each of which contains layer info and a openlayers ol.layer.Tile object
 * each layer created is automatically added to the main map
 */

// Default proxy geopackage service, available for tileserver's that don't natively support geopackages
var globals = {
    geopackageServiceRoot: 'https://' + window.location.host + '/geopackage/v1'      // if a tileserver doesn't support geopackages, use this proxy
};

/*
 * main layers object
 * contains layer list ( of layer objects ), and
 * layer constructor
 */
var layers = {
    services: [],
    geopackageServers: [],
    layerList: {},
    //constructor
    layer: function (name, title, service) {
        this.name = name;
        this.title = title;
        this.url = service.url;

        this.protocol = service.protocol;
        this.serviceId = service.id;
        this.featureData = false; // this will have to be expanded on if we start supporting vector layers like WFS
        this.selected = false;
        if (service.categories){
            this.category = layers.getLayerCategory(name, service.categories);
        }
        else{
            this.category = "";
        }
        if(service.geopackageServer){
            this.geopackageServer = service.geopackageServer;
        }
        else{
            this.geopackageServer = globals.geopackageServiceRoot;
        }
        //open layers 3 layer
        this.layer = new ol.layer.Tile({
            title: title,
            source: new ol.source.TileWMS({
                url: service.url,
                params: {
                    LAYERS: name,
                    VERSION: '1.1.1',
                    TILED: true
                }
            }),
            visible: false
        });
        this.toggle = function() {
            if (this.selected) {
                this.selected = false;
                this.layer.setVisible(false);
            } else {
                this.selected = true;
                this.layer.setVisible(true);
            }
        };
        //add immediately to map
        map.addLayer(this.layer);
    }
};

/**
 * Retrieve the category of a layer. 
 * Matches name against strings in categories array
 * @param {*} name 
 * @param {*} categories 
 * @return {String}
 */
layers.getLayerCategory = function (name, categories){
    for(var i = 0; i < categories.length; i++){
        var tempName = name.toLowerCase();
        for(var e = 0; e < categories[i][1].length; e++){
            if(tempName.indexOf(categories[i][1][e]) > -1 ){
                return categories[i][0];
            }
        }
    }
    return "Other";
}

/**
 * Retrieve all layers for given category and service
 * @param {*} category 
 * @param {*} serviceId 
 * @return {layers}
 */
layers.getLayersByCategory = function (category, serviceId){
    var categoryLayers = [];
    for (l in layers.layerList){
        if (layers.layerList[l].serviceId == serviceId && layers.layerList[l].category == category){
            categoryLayers.push(layers.layerList[l]);
        }
    }
    return categoryLayers;
}

/**
 * Retrieve all layers for given service id
 * @param {*} category 
 * @param {*} serviceId 
 * @return {layers}
 */
layers.getLayersByService = function (serviceId){
    var serviceLayers = [];
    for (l in layers.layerList){
        if (layers.layerList[l].serviceId == serviceId){
            serviceLayers.push(layers.layerList[l]);
        }
    }
    return serviceLayers;
}


/**
 * Retrieve the WMS XML and pass values to buildLayers();
 * called immediately
 */
layers.getWMSLayers = function (service) {
    $.ajax({
        type: 'GET',
        url: service.url + "?REQUEST=GetCapabilities&VERSION=1.1.1&SERVICE=WMS",
        dataType: 'xml',
        success: function (xml) {
            layers.addLayers(xml, service);
            service.loaded = true;
        },
        error: function () {
            alert('error: failed to retrieve layers XML');
            service.error = true;
        }
    });
};

/**
 * parse throughout xml to find layer name and title
 * create layer and add to allTheLayers with bracket syntax (makes it easier to call from select list)
 * add to map
 */
layers.addLayers = function (xml, service) {
    //foreach loop to add all the layers
    $(xml).find('Layer Layer').each(function (i, layer) {
        var layerName = $(layer).find('Name').html();
        var layerTitle = $(layer).find('Title').html();

        //create new layer object, add to layers.layerList list
        layers.layerList[layerName] = new layers.layer(layerName, layerTitle, service);
    });

};

/**
 * Import services from layers.json file
 * update json object with new format for categories and pass to 
 *      getWMSLayers function to populate layerList object
 * add service to services array
 */
$.getJSON('layers.json', function(data) {
    data.services.forEach( function(service, index){

        //test for relative urls
        if(service.relative){
            let host = window.location.host;
            // host = "nrltileserver.cs.uno.edu";
            service.url = "https://" + host + service.url;
            if(service.geopackageServer){
                service.geopackageServer = "https://" + host + service.geopackageServer;
            }
        }

        //process categories
        if (service.categories){
            service.categories.forEach(function(cat){
                var stringArray = cat[1].replace(/\s/g, ''); // remove spaces
                stringArray = stringArray.toLowerCase();
                stringArray = stringArray.split(',');
                cat[1] = stringArray;
            });
            service.categories.push(["Other","Other"]);
            service.hasCategories = true;
        }
        //add id
        service.id = index;

        // add 'loaded' flag
        service.loaded = false;
        service.error = false;
        if(service.protocol.toLowerCase() == "wms"){
            layers.getWMSLayers(service);
        }
        //add geopackageServers. don't include duplicates
        if(!service.geopackageServer){
            service.geopackageServer = globals.geopackageServiceRoot;
        }
        if(layers.geopackageServers.findIndex((e)=>{return e.url === service.geopackageServer}) === -1){
            layers.geopackageServers.push({url: service.geopackageServer});
        }

        layers.services.push(service);

    } );
});