const TILE_CODE = {

    /*
     * get the column number for a given coordinate and zoom level
     * @param {number} coordinate
     * @param {integer} zoom level
     * @return {integer}
     */
    getColForCoordZoom(lon, zoom) {
        var coord = lon + 180;
        // get total # of cols for this zoom level (not all tiles)
        var cols = Math.pow(2, (zoom));
        var col = coord * cols / 360;
        col = Math.ceil(col);
        return col;
    },

    /*
     * get the row number for a given coordinate and zoom level
     * @param {number} coordinate
     * @param {integer} zoom level
     * @return {integer}
     */
    getRowForCoordZoom(lat, zoom) {
        var coord = lat + 90;
        // get total # of rows for this zoom level
        var rows = Math.pow(2, (zoom - 1));
        var row = coord * rows / 180;
        row = Math.ceil(row);
        return row;
    },

    /* 
     * get total number of tiles for current zoom level
     */
    getNumTiles(zoom, bbox) {
        const mincol = this.getColForCoordZoom(bbox.minx, zoom);
        const maxcol = this.getColForCoordZoom(bbox.maxx, zoom);
        const minrow = this.getRowForCoordZoom(bbox.maxy, zoom);
        const maxrow = this.getRowForCoordZoom(bbox.miny, zoom);

        return (Math.abs(maxcol - mincol) + 1) * (Math.abs(maxrow - minrow) + 1);
    },    

    /* !new
     * get total number of tiles for the requested 
     * geopackage, including all zoom levels
     */
    getNumTotalTiles(minZoom, maxZoom, bbox) {
    	let numTiles = 0;
    	for (let i = minZoom; i <= maxZoom; i++) {
    		numTiles += this.getNumTiles(i, bbox);
    	}
    	return numTiles;
    },
}

// TILE_CODE.getNumTotalTiles(0, 1, {minx:-180,maxx:180,maxy:90,miny:-90})
