/*
 * Geopackage Downloader - Angular app
 * Imports layers from NRL WMS Tileserver and displays them in OpenLayers 3 map
 * provides tools to configure paremeters for downloading a Geopackage
 */

const app = angular.module('gpkg-app', []);

app.controller('AppController', function() {
    this.map = map; // openlayers map instance
    this.sidebarClass = 'show-sidebar';

    //outline tiles with css
    this.outlineClass = 'no-outlines';

    /**
     * shows / hides the sidebar
     */
    this.toggleSidebar = function() {
        if (this.sidebarClass == 'show-sidebar') {
            this.sidebarClass = 'hide-sidebar';
        } else {
            this.sidebarClass = 'show-sidebar';
        }
        setTimeout(function() { map.updateSize(); }, 0);
    };

    /**
     * Toggles tile outlines - disabled in this version
     */
    this.toggleOutlines = function() {
        if (this.sidebarClass == 'no-outlines') {
            this.sidebarClass = 'outlines';
        } else {
            this.sidebarClass = 'no-outlines';
        }
    };
});

app.controller('NavController', function($scope) {
    //show dropdown menu
    $scope.menu = false;
});

app.directive('appDirective', ['$sce', ($sce) => ({
    restrict: 'E',
    templateUrl: 'templates/geopackage.html',
    controller: ['$scope', '$http', '$interval', function(sc, $http, $interval) {
        const ctrl = this;

        // tabbed navigation for displaying 'edit geopackage' and 'downloads' tabs
        this.showNav = false;
        this.curNav = 'main';

        //change current tab and scroll to top
        this.setNav = function(nav){
            this.curNav = nav;
            //$('.scrollablePanel').mCustomScrollbar("scrollTo","top");
        };

        this.scope = sc;

        this.box = {
            minx: getExtent()[0],
            miny: getExtent()[1],
            maxx: getExtent()[2],
            maxy: getExtent()[3],
        };

        this.taskIds = [];
        this.tasksInProgress = [];

        this.scaleRatio = [316000000, 158000000, 74000000, 37000000, 18000000, 9000000, 5000000, 2000000,
            1000000, 577000, 288000, 144000, 72000, 36000, 18000, 9012, 4506, 2222
        ];
        this.currentScale = this.scaleRatio[Math.round(map.getView().getZoom())];
        this.currentZoom = map.getView().getZoom();
        this.maxZoom = 2;
        this.minZoom = 2;
        this.profiles = [];
        this.profile = {};
        this.geopackageProfiles = [];
        this.setBboxManually = false; // set to true when a user draws a bbox or alters values in bbox input fields
        this.profile = "";
        this.setBboxManually = false;
        this.setZoomManually = false;
        this.sizeWarningConfirmation = false;
        this.displaySizeWarning = false; // set automatically
        this.hasFileSizeLimit = false; // set by user
        this.fileSizeLimit = 500; // in MB
        this.email = "";
        this.showTestIcon = false;

        this.geopackageServers = layers.geopackageServers;
        //this.layerMenu = [];

        // if the drawn bounding box crosses the international dateline
        this.crossesDateLine = false;
        this.crossesDateLineLeft = false;

        /* 
         * #Validations
         * contains methods for check form validity as well as a collection
         * of "Condition" objects for each element we need to validate
         */
        this.validation = {
            validate: () => {
                var valid = true;
                for (var prop in this.validation.conditions) {
                    if (this.validation.conditions[prop].test() == false) {
                        valid = false;
                    }
                }
                if (valid == false) {
                    this.validation.showAllErrors();
                    this.validation.toggleFormError();
                }

                return valid;
            },
            showAllErrors: () => {
                for (var prop in this.validation.conditions) {
                    this.validation.conditions[prop].show = true;
                }
            },
            toggleFormError: () => {
                this.validation.showFormError = true;
                setTimeout(() => {
                    this.validation.showFormError = false;
                    ctrl.scope.$apply();
                }, 4000);
            },
            showFormError: false,
            /*
             * Form Validation Conditions
             * Params
             *    validation condition (boolean)
             *    html error message (String)
             *    display error before form is submitted (boolean))
             */
            conditions: {
                hasLayer: new Condition(() => {
                        return ctrl.getNumLayers() > 0
                    },
                    $sce.trustAsHtml("Please select a layer"), false),
                latMinMax: new Condition(() => {
                        return this.box.miny < this.box.maxy && this.box.miny != this.box.maxy
                    },
                    $sce.trustAsHtml("<b>Min Latitude cannot be greater than or equal to Max Latitute</b>"), true),
                lonMinMax: new Condition(() => {

                        return this.box.minx >= -180 && this.box.maxx <= 180
                    },
                    $sce.trustAsHtml("<b>Out of bounds! Selection cannot exceed 360 degrees</b>"), true),
                scaleMinMax: new Condition(() => {
                        return this.minZoom <= this.maxZoom
                    },
                    $sce.trustAsHtml("Max Scale must be higher resolution (deeper zoom) than Min Scale"), true),
                fileSizeLimit: new Condition(() => {
                        return this.hasFileSizeLimit == false || (this.getSizeEstimate() / 1000) < this.fileSizeLimit
                    },
                    $sce.trustAsHtml("Estimated geopackage file size exceeds the set limit"), true)
            }
        };

        /* Constructor for validation conditions */
        function Condition(test, message, show) {
            this.test = test;
            this.message = message;
            this.show = show;
        }

        /* 
         * set min/max Zoom to the current map's zoom level
         * when users selects the "Set" buttons next to the min/max zoom inputs
         */
        this.setMinZoom = function() {
            this.minZoom = map.getView().getZoom();
            this.minScale = "1:" + this.scaleRatio[this.minZoom].toLocaleString();
            this.setZoomManually = true;
        };
        this.setMaxZoom = function() {
            this.maxZoom = map.getView().getZoom();
            this.maxScale = "1:" + this.scaleRatio[this.maxZoom].toLocaleString();
            this.setZoomManually = true;
        };
        this.resetZoom = function() {
            this.setZoomManually = false;
            this.minZoom = map.getView().getZoom();
            this.minScale = "1:" + this.scaleRatio[this.minZoom].toLocaleString();
            this.maxZoom = map.getView().getZoom();
            this.maxScale = "1:" + this.scaleRatio[this.maxZoom].toLocaleString();
        };
        //init
        this.resetZoom();

        /* 
         * calculates Degrees Per Pixel for a given zoom level
         * @param {number} zoom level
         * @return {number} DPP
         */
        function getDppFromZoom(zoom) {
            var curZoom = map.getView().getZoom();
            var curDpp = map.getView().getResolution();
            if (zoom == curZoom) {
                return curDpp / 2;
            } else if (zoom > curZoom) {
                return curDpp / Math.pow(2, (zoom - curZoom - 1));
            } else {
                return curDpp * Math.pow(2, (curZoom - zoom - 1));
            }
        }

        /* ---- SIZE ESTIMATES ---------------------------------------------------------------------------------------------------- */
        /**
         * Estimates number of tiles that will be generated by geopackage request
         * @returns {number}
         */
        this.getNumTiles = function() {
            if (ctrl.box) {
                return TILE_CODE.getNumTotalTiles(ctrl.minZoom, ctrl.maxZoom, ctrl.box) * this.getNumLayers();
            }
            return 0;
        };

        /*
         * return a very rough estimate of the size
         * of the requested geopackage in kilobytes
         * @return {string} unit suffix: KB, MB, GB
         */
        this.getSizeEstimate = function() {
            //get total number of layers to multiply estimate by
            //If WFS or other Feature layers are included in the future, this will need to be updated
            return this.getNumTiles() * 100;
        };

        /*
         * builds a string based on the current size estimate,
         * adding the appropriate unit at the end
         * @return {String} size estimate text
         */
        this.displaySizeEstimate = function() {
            var size = this.getSizeEstimate();
            var unit = "KB";
            var estimate = "";
            if (size >= 1000 && size < 1000000) {
                size = size / 1000;
                unit = "MB";
            } else if (size >= 1000000) {
                size = size / 1000000;
                unit = "GB";
            }
            estimate = size + " " + unit;
            return estimate;
        };

        /*
         * Sets the Geopackage size limit warning to true, hides the size limit confirmation panel from the view
         * and submits the form
         */
        this.confirmSizeWarning = () => {
            this.sizeWarningConfirmation = true;
            this.displaySizeWarning = false;
            this.submitTask();
        };

        /*
         * checks to see if the selected geopackage will be larger than 1GB
         * @return {boolean}
         */
        this.sizeCheck = () => {
            if (this.getSizeEstimate() > 1000000 && this.sizeWarningConfirmation == false) {
                this.displaySizeWarning = true;
                return false;
            } else {
                return true;
            }
        };

        //get profiles from geoserver
        this.getProfiles = () => {
            this.geopackageServers.forEach((server)=>{
                $http({
                    method: 'GET',
                    url: server.url + '/profiles'
                }).then(function successCallback(response) {
                        server.profile = response.data.defaultKey;
                        server.profiles = response.data.profiles;
                    },
                    function errorCallback(response) {
                        server.profile = "DEFAULT"
                        server.profiles[0] = { "DEFAULT": "Default" };
                    });
            });
        };

        //init profiles
        this.getProfiles();

        /*
         * returns the current zoom level of the map
         */
        this.getZoom = function() {
            return map.getView().getZoom();
        };

        /*
         * set the Bbox to the map's current extent
         */
        this.resetBbox = () => {
            var curExtent = getExtent();
            this.box = {
                minx: curExtent[0],
                miny: curExtent[1],
                maxx: curExtent[2],
                maxy: curExtent[3],
            };
            vector.getSource().clear();
            this.setBboxManually = false;
        };


        /* ----- END SIZE ESTIMATES --------------------------------------------------------------------------------------------------------------------*/


        /* FORM SUBMISSION -------------------------------------------------------------------------------------------
         *
         */

        //array to store potential server errors in
        this.serverErrors = [];

        //testing- output to js log
        this.submitTest = () => {
            if (this.sizeCheck() && this.validation.validate()) {
                console.log(this.getJobJson());
            }
        };

        /**
         * Validates then submits the geopackage jobs to the server(s) specified in
         * the layers.json config.
         * Updates view to display "Downloads" tab
         */
        this.submitTask = () => {

            if (this.sizeCheck() && this.validation.validate()) {
                const requestConfig = {
                    headers: {
                        'Content-Type': 'text/plain',
                    },
                };

                //switch tab to show downloads/errors
                ctrl.showNav = true;
                ctrl.setNav('downloads');
                const jobJson = ctrl.getJobJson();

                for (let i=0; i < jobJson.length; i++){
                    let gpkgService = jobJson[i].layers[0].properties.geopackageServer;

                    $http.post(gpkgService, jobJson[i], requestConfig)
                        .then((response) => {
                            const uuid = response.data.id;
                            const timing = 1000;
                            // angular interval for checking task progress
                            const worker = $interval(() => {
                                $http.get(`${gpkgService}/status/${uuid}`)
                                    .then((statusResponse) => {
                                        if (response.status === 200) {
                                            ctrl.update({
                                                uuid,
                                                serviceUrl: gpkgService,
                                                status: statusResponse.data,
                                                worker,
                                            });
                                        }
                                    }, (statusResponse) => {
                                        //request failed, exit loop
                                        console.log("Server Error In Loop");
                                        console.log(statusResponse);
                                        ctrl.serverErrors.push(response);
                                        $interval.cancel(worker);
                                    });
                            }, timing);
                        }, (response) => {
                            //request failed
                            console.log("Server Error: ");
                            console.log(response);
                            ctrl.serverErrors.push(response);
                        });
                }
            }
        };

        /**
         * update task status. if job is complete, update download link
         * @param task
         */
        this.update = (task) => {
            for (let oldTask of ctrl.tasksInProgress) {
                if (oldTask.uuid === task.uuid) {
                    oldTask.status.doneTiles = task.status.doneTiles;
                    if (task.status.failed) {
                        console.log("task failed");
                        oldTask.status.failed = task.status.failed;
                        if (angular.isDefined(task.worker)) {
                            $interval.cancel(task.worker);
                        }
                    } else if (task.status.done) {
                        oldTask.status.done = task.status.done;
                        oldTask.status.downloadLink = task.serviceUrl + '/download/' + task.status.id;
                        if (angular.isDefined(task.worker)) {
                            $interval.cancel(task.worker);
                        }
                    }
                    return;
                }
            }

            // if we make it here, it wasn't in the list already, add it
            ctrl.tasksInProgress.push(task);
        };

        /*
        * Create JSON to submit to the geopackage service
        * @return {JSON}
        */
        this.getJobJson = () => {
            //for testing, this job will fail on the server
            //const job = {"bbox":{"minX":-31.9922,"maxX":11.4258,"maxY":22.7637,"minY":-7.6465},"layers":[{"name":"basemapHillshade","featureData":false,"properties":{"serviceName":"basemapHillshade","protocol":"ArcGISMapServer","url":"//services.arcgisonline.com/arcgis/rest/services/NGS_Topo_US_2D/MapServer"}}],"imageFormat":"png","profile":"DEFAULT","minResDpp":0.17021267819429778,"maxResDpp":0.17021267819429778};
            var jobLayers = this.getJobLayers(); // joblayers, separated by geopackageDownloader server

            var jobs = [];
            for (let i=0; i< jobLayers.length; i++){
                jobs.push({
                    bboxes: ctrl.getJobBboxes(),
                    layers: jobLayers[i].layers,
                    profile: jobLayers[i].profile,
                    minResolutionDpp: getDppFromZoom(ctrl.minZoom),
                    maxResolutionDpp: getDppFromZoom(ctrl.maxZoom)
                });
                if ( ctrl.email != "" ){ jobs[i].email = ctrl.email; }

            }
            return jobs;
        };

        /**
         * Gets the bbox specified by the user
         * Checks for selections crossing the anti-meridian and adjusts bbox coordinates as needed
         * @returns {{minx: (*|number), maxx: (*|number), maxy: (*|number), miny: *}[]|*[]}
         */
        this.getJobBboxes = () => {
            //if maxx > 180 (bbox crosses the international date line), then we have to create 2 bboxes
            var bboxes;
            if ( ctrl.crossesDateLine ) {
                bboxes = [{
                    minx: ctrl.box.minx,
                    maxx: 180,
                    maxy: ctrl.box.maxy,
                    miny: ctrl.box.miny,
                },{
                    minx: -180,
                    maxx: ctrl.box.maxx,
                    maxy: ctrl.box.maxy,
                    miny: ctrl.box.miny,
                }];
            }
            else {
                bboxes = [{
                    minx: ctrl.box.minx,
                    maxx: ctrl.box.maxx,
                    maxy: ctrl.box.maxy,
                    miny: ctrl.box.miny
                }];
            }
            return bboxes;
        };

        /* ------------------------------------------------------------------------------------------------------------
         * Openlayers ui
         * Add bounding box, layer for box (vector), interaction (draw),
         * and methods to update the bbox layer
         * ------------------------------------------------------------------------------------------------------------
         */
        function getExtent() {
            var extent = map.getView().calculateExtent(map.getSize());
            if (extent[0] < -180) { extent[0] = -180; }
            if (extent[1] < -90) { extent[1] = -90; }
            if (extent[2] > 180) { extent[2] = 180; }
            if (extent[3] > 90) { extent[3] = 90; }
            return extent;
        }

        // Bounding Box: array of coordinates populated by draw event
        var bbox;

        /*
         * Create vector layer and drawing interaction
         * Set coordinate inputs at end of drawing action
         */
        var source = new ol.source.Vector({ wrapX: false });
        var vector = new ol.layer.Vector({
            source: source,
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#ffcc33',
                    width: 2
                }),
                image: new ol.style.RegularShape({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: '#ffcc33'
                    })
                })
            })
        });

        //add new vector layer to map, set it's zindex hight
        map.addLayer(vector);
        vector.setZIndex(1000);

        /* 
         * Add Draw interaction
         * before draw, clear previous drawing
         * after draw, write min/max lat/lon values to html inputs
         */
        var draw;

        function addInteraction() {
            var geometryFunction, maxPoints;

            value = 'LineString';
            maxPoints = 2;
            geometryFunction = function(coordinates, geometry) {
                if (!geometry) {
                    geometry = new ol.geom.Polygon(null);
                }
                var start = coordinates[0];
                var end = coordinates[1];
                geometry.setCoordinates([
                    [start, [start[0], end[1]], end, [end[0], start[1]], start]
                ]);
                return geometry;
            };

            draw = new ol.interaction.Draw({
                source: source,
                type: /** @type {ol.geom.GeometryType} */ (value),
                geometryFunction: geometryFunction,
                maxPoints: maxPoints
            });

            map.addInteraction(draw);

            draw.on('drawend', function(e2) {
                var feature = e2.feature;
                var geometry = feature.getGeometry();
                var coordinates = geometry.getCoordinates()[0];
                bbox = coordinates;
                ctrl.box.minx = bbox[0][0];
                ctrl.box.maxx = bbox[2][0];
                ctrl.box.miny = bbox[1][1];
                ctrl.box.maxy = bbox[0][1];
                ctrl.sortBbox();
                ctrl.setBboxManually = true;
                sc.$apply();
            });

            draw.on('drawstart', function() {
                source.clear();
            });
        }
        //init interaction
        addInteraction();

        /** 
         * Called on drawend
         * Calculate the appropriate coordinates for this.box after the users draws a bbox
         * min/max coordinates can be switched if a user draws a box bottom to top or right to left, these must be switched
         * 
         * care must be taken if the bounding box crosses the international dateline
         *  * if one coordinate crosses the dateline, set the crossesDateLine flag (2 bboxes will be required)
         *  * if 2 coordinates cross the dateline, adjust their bounds 
         *      if the resulting minx > maxx, set the crossesDateLine flag
         */
        this.sortBbox = () => {
            this.crossesDateLine = false;
            
            //flip values
            if (this.box.minx > this.box.maxx) {
                var tempX = this.box.minx;
                this.box.minx = this.box.maxx;
                this.box.maxx = tempX;
            }
            if (this.box.miny > this.box.maxy) {
                var tempY = this.box.miny;
                this.box.miny = this.box.maxy;
                this.box.maxy = tempY;
            }

            //contain latitude
            if (this.box.maxy > 90){
                this.box.maxy = 90;
                setTimeout(() => {
                    ctrl.updateBboxVector();
                }, 100);
            }
            if (this.box.miny < -90){
                this.box.miny = -90;
                setTimeout(() => {
                    ctrl.updateBboxVector();
                }, 100);
            }

            // test for international dateline crossing. It is allowed, but 
            // we need to set a flag and will ultimately submit 2 bboxes
            if ( ( this.box.minx < -180 && this.box.maxx > 180 ) || ( this.box.maxx < -180 && this.box.minx > 180 ) ){
                //error
                console.log("bbox exceeds the circumference of the earth");
            }
            //crosses dateline left
            else if ( this.box.minx < -180 && this.box.maxx > -180 ){
                this.crossesDateLine = true;
                this.crossesDateLineLeft = true;
                this.box.minx = this.box.minx + 360;
            }
            //crosses dateline Right
            else if ( this.box.maxx > 180 && this.box.minx < 180 ){
                this.crossesDateLine = true;
                this.crossesDateLineLeft = false;
                this.box.maxx = this.box.maxx - 360;
            }
            //both values out of bounds
            else if ( ( this.box.minx < -180 && this.box.maxx < -180 ) || ( this.box.minx > 180 && this.box.maxx > 180 ) ){
                this.box.minx = this.adjustCoordBounds(this.box.minx);
                this.box.maxx = this.adjustCoordBounds(this.box.maxx);
                if (this.box.minx > this.box.maxx){ // crosses dateline
                    this.crossesDateLine = true;
                }
            }
        };

        /**
         * corrects longitudinal coordinates when both minx and maxx are greater than 180 
         * or when both are less than -180
         * @param {float}
         * @return {float}
         */
        this.adjustCoordBounds = (c) => {
            if ( c < -180 ){
                return 180 - Math.abs( (c + 180) % 360 );
            }
            else if ( c > 180 ){
                return ( ( c + 180 ) % 360 ) - 180;
            }
        };

        /**
         * Converts bounding box object into an Openlayers coordinate object: 
         * @param {object} - bounding box object: {minx, maxx, miny, maxy}
         * @return {object}
         */
        this.convertBbboxToCoords = (box) => {
            var coords = [
                [
                    [
                        [box.minx], [box.maxy]
                    ],
                    [
                        [box.minx], [box.miny]
                    ],
                    [
                        [box.maxx], [box.miny]
                    ],
                    [
                        [box.maxx], [box.maxy]
                    ],
                    [
                        [box.minx], [box.maxy]
                    ]
                ]
            ];
            return coords;
        };

        /**
         * Get the bbox
         * @return {object} bounding box object: {minx, maxx, miny, maxy}
         */
        this.getBox = () => {
            var box = {minx: this.box.minx, miny: this.box.miny, maxx: this.box.maxx, maxy: this.box.maxy};
            return box;
        };

        /** 
         * update bounding box drawing when user changes the bbox coordinates manually
         * if min longitude is greater than max longitude, the drawing crosses the date line
         * if no drawing yet exists, create one
         */
        this.updateBboxVector = () => {
            var box = this.getBox();
            if (this.box.minx > this.box.maxx){ // assume they meant to do this and bbox should cross dateline
                if (this.crossesDateLineLeft == true){ // crosses dateline on left side (based on current drawing)
                    box.minx = box.minx - 360;
                }
                else{
                    box.maxx = box.maxx + 360;
                }
            }
            var coords = this.convertBbboxToCoords(box);
            if (vector.getSource().getFeatures()[0]){
                vector.getSource().getFeatures()[0].getGeometry().setCoordinates( coords );
            }
            else{ // create new drawing
                vector.getSource().clear();
                var geometry = new ol.geom.Polygon(null);
                geometry.setCoordinates( coords );
                var feature = new ol.Feature(geometry);
                vector.getSource().addFeature(feature);
            }
            this.setBboxManually = true;
        };

        //called when map is zoomed in or out
        map.getView().on('change:resolution', function(e) {
            if (!ctrl.setZoomManually) {
                ctrl.resetZoom();
            }
            sc.$apply();
        });

        // called when map is moved (browser resize, zoom)
        map.on('moveend', function(e) {
            if (!ctrl.setBboxManually) {
                ctrl.resetBbox();
            }
            sc.$apply();
        });


        /* ------------------------------------------------------------------------------------------------------------
         * END OPEN LAYERS CODE
         * ------------------------------------------------------------------------------------------------------------
         */

        this.showLayers = false;

        this.services;

        /* LAYER MANAGEMENT ------------------------------------------------------------------------------------- */

        /*
         * check current $apply phase to avoid "already in progress" scope errors
         * required for handling an ajax calls that are out of angular's scope (particularly from layers.js)
         */
        sc.safeApply = function(fn) {
            var phase = this.$root.$$phase;
            if (phase === '$apply' || phase === '$digest') {
                if (fn && (typeof(fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };


        //import all the layers and update the scope
        this.addLayers = () => {
            this.services = layers.services;
            this.getLayersByCategory = layers.getLayersByCategory;
            this.getLayersByService = layers.getLayersByService;
            this.layers = layers.layerList;
            ctrl.showLayers = true;
            sc.safeApply(() => console.log("wms layers have loaded"));
        };


        /**
         * Checks to see if all layers have loaded on an interval
         * //we can't import the layers until the 'layers' object has finished importing them from the WMS server
         */
        this.checkLayerStatus = () => {
            var layersHaveLoaded = true;
            layers.services.forEach((e)=>{
                if(!e.loaded && !e.error){
                    layersHaveLoaded = false;
                }
            });
            if (!layersHaveLoaded){
                setTimeout(this.checkLayerStatus, 50);
            }
            else{
                this.addLayers();
            }
        };

        //init checkLayerStatus
        this.checkLayerStatus();




        /**
         * Takes a layer object (as defined in layers.js) and returns a layer object
         * configured for a geopackageDownloader server
         * @param layer
         * @returns {{name: *, featureData: boolean, properties: {serviceName: *, protocol: (*|string|RTCIceProtocol), url: *, geopackageServer: *}}}
         */
        function getJobLayer(layer){
            return ({
                name: layer.name,
                featureData: layer.featureData,
                properties: {
                    serviceName: layer.name,
                    protocol: layer.protocol,
                    url: layer.url,
                    geopackageServer: layer.geopackageServer
                }
            });
        }

        /**
         * Iterates over user-selected layers, creating new layer objects configured for submission to geopackageDownloader servers
         * New layer objects are grouped by server
         * @returns {Array}     - url: address of geopackageDownloader,
         *                      - layers: array of layer objects,
         *                      - profile: gpkg profile (specified by the geopackageDownloader server)
         */
        this.getJobLayers = () => {
            var jobServers = [];
            for (var e in this.layers){
                if (this.layers[e].selected) {
                    // does the corresponding service exist in jobServices yet?
                    if (jobServers.length === 0){ // add first server
                        jobServers.push({
                            url: this.layers[e].geopackageServer,
                            layers: [getJobLayer(this.layers[e])],
                            profile: this.geopackageServers.find((server) => {return server.url === this.layers[e].geopackageServer}).profile
                        });
                    }
                    else {
                        let serverIndex = jobServers.findIndex((server) =>{return server.url === this.layers[e].geopackageServer});
                        if (serverIndex === -1){ // server doesn't exist yet, add it
                            jobServers.push({
                                url: this.layers[e].geopackageServer,
                                layers: [getJobLayer(this.layers[e])],
                                profile: this.geopackageServers.find((server) => {return server.url === this.layers[e].geopackageServer}).profile
                            });
                        }
                        else{ // add layer to existing server
                            jobServers[serverIndex].layers.push(getJobLayer(this.layers[e]));
                        }
                    }
                }
            }
            return jobServers;
        };

        /*
         * get the number of layers that have been selected
         */
        this.getNumLayers = () => {
            var numLayers = 0;
            for (e in this.layers) {
                if (this.layers[e].selected) {
                    numLayers++;
                }
            }
            return numLayers;
        };

        /* end layer management ----------------------------------------------------------------------------- */

    }], //end controller for appDirective
    controllerAs: 'ctrl',

})]);

app.directive('layerMenu', () => {
    return{
        templateUrl: "templates/layerMenu.html"
    };
});

