/**
 * object that holds all the layers
 * populated from parse xml via buildLayers()
 */


/* ------------------------------------------------------------------------------------------------------------
 * WMS Server and layer to use for the map's baselayer
 */
var mapGlobals = {
    baseMapServer: 'https://' + window.location.host + '/nrltileserver/wms',
    baseMapLayer: 'BlueMarble_January'
};

/* ------------------------------------------------------------------------------------------------------------
 * BASE LAYER
 */
var baseLayer = new ol.layer.Tile({
    title: 'Global Imagery',
    source: new ol.source.TileWMS({
        url: mapGlobals.baseMapServer,
        params: { LAYERS: mapGlobals.baseMapLayer, VERSION: '1.1.1', TILED: true },
    }),
});

var centerUnitedStates = [-98.35, 39.50];

/* ------------------------------------------------------------------------------------------------------------
 * Build Map
 */
var map = new ol.Map({
    //layers: [baseLayer, layer4, layer3, layer2, layer1, vector],
    // layers: [],
    layers: [baseLayer],
    target: 'map',
    view: new ol.View({
        projection: 'EPSG:4326',
        center: centerUnitedStates,
        zoom: 4
    }),
    renderer: 'dom'
});

/* ------------------------------------------------------------------------------------------------------------
 * Interactions
 */

//controls
var mousePosition = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(2),
    projection: 'EPSG:4326',
    target: document.getElementById('mouse-position'),
    undefinedHTML: '&nbsp;'
});
map.addControl(mousePosition);

//add an extent control: resets the zoom/center start point
var zoomResetLabel = document.createElement("SPAN");
zoomResetLabel.className += " glyphicon glyphicon-globe";

var zoomToExtentControl = new ol.control.ZoomToExtent({
    extent: [-90, -90, 90, 90],
    label: zoomResetLabel,
    tipLabel: "Reset Zoom"
});
map.addControl(zoomToExtentControl);

var scaleLine = new ol.control.ScaleLine();
map.addControl(scaleLine);

// this conflicts with the draw interaction. you could put a timeout on the single click so it will default
// to "draw" mode if it isn't followed by a double click within, say, 200ms.
//map.addInteraction(new ol.interaction.DoubleClickZoom(opt_options));


/**
 * Updates the bounding box drawing on the vector layer
 * Typically after coordinate input box has been changed
 * Params: coordinate array [[[],[],[],[],[]]]
 */
// function updateGeometry(coords) {
//     vector.getSource().clear();
//     var geometry = new ol.geom.Polygon(null);
//     geometry.setCoordinates(coords);
//     var feature = new ol.Feature(geometry);
//     vector.getSource().addFeature(feature);
// }
